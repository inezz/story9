from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
import json
from .models import Books
from django.http import JsonResponse

# Create your views here.
def index(request):
    return render(request, 'index.html')

@csrf_exempt
def like(request):
    result = json.loads(request.body)
    try:
        book_result = Books.objects.get(book_id=result['id'])
        book_result.like_count += 1
        book_result.save()
    except:
        book_result = Books.objects.create(
            book_id=result['id'],
            image_link =result['volumeInfo']['imageLinks']['thumbnail'],
            title=result['volumeInfo']['title'],
            authors="Unknown" if 'authors' not in result['volumeInfo'] else result['volumeInfo']['authors'],
            publisher=result['volumeInfo']['publisher'],
            like_count=1
        )
    return JsonResponse({
        'like' : book_result.like_count
    })

@csrf_exempt
def dislike(request):
    result = json.loads(request.body)
    try:
        book_result = Books.objects.get(book_id=result['id'])
        if book_result.like_count > 0:
            book_result.like_count -= 1
        book_result.save()
    except:
        book_result = Books.objects.create(
            book_id=result['id'],
            image_link =result['volumeInfo']['imageLinks']['thumbnail'],
            title=result['volumeInfo']['title'],
            authors="Unknown" if 'authors' not in result['volumeInfo'] else result['volumeInfo']['authors'],
            publisher=result['volumeInfo']['publisher'],
            like_count = 0
        )
    return JsonResponse({
        'like' : book_result.like_count
    })

def top5Books(request):
    book = Books.objects.order_by('-like_count')
    book_list = []
    for i in book:
        book_list.append({
            'image_link' : i.image_link,
            'title' : i.title,
            'authors' : i.authors,
            'publisher' : i.publisher,
            'like' : i.like_count,
        })
    return JsonResponse({
        'book_list' : book_list
    })

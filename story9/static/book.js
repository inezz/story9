var books = [];

$(document).ready(function(){
      $("input").keyup(
        function(){
          var title = $("#searchField").val()
          var xhttp = new XMLHttpRequest()
          console.log("start", title)
          xhttp.onreadystatechange = function(result){
            if(this.readyState == 4 && this.status == 200) {
              var result = JSON.parse(this.responseText);
              books = result.items;
              $("table").empty();
              $("table").append(
                '<thead>' +
                  '<tr>' +
                    '<th scope="col" class="table-heading align-middle">Cover</th>' +
                    '<th scope="col" class="align-middle">Title</th>' +
                    '<th scope="col" class="align-middle">Author</th>' +
                    '<th scope="col" class="align-middle">Publisher</th>' +
                    '<th scope="col" class="align-middle">Like</th>' +
                    '<th scope="col" class="align-middle">Dislike</th>' +
                    '<th scope="col" class="align-middle">Like Count</th>' +
                  '</tr>'+
                '</thead>'
              );
              for(i = 0; i < result.items.length ; i++){
                $("table").append(
                  "<tbody>" +
                    "<tr>" +
                      "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                      "<td id='title'>" + result.items[i].volumeInfo.title + "</td>" +
                      "<td id='author'>" + result.items[i].volumeInfo.authors + "</td>" +
                      "<td>" + result.items[i].volumeInfo.publisher + "</td>" +
                      "<td class='align-middle'>" + "<button id='like-button' type='button' class='mybtn' onclick='likeBook(" + i + ")'> ❤️ </button>" + "</td>" +
                      "<td class='align-middle'>" + "<button id='dislike-button' type='button' class='mybtn' onclick='dislikeBook(" + i + ")'> 💔 </button>" + "</td>" +
                      "<td class='align-middle' id='like-counter'>" + "<p id='likes" + i + "' data-value='0'></p>" + "</td>" +
                    "</tr>" +
                  "</tbody>"
                );
              }
            }
          }
      xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=%22" + title, true);
      xhttp.send();
    })
  }
); 

function likeBook(i) {
  var result = JSON.stringify(books[i]);
  var xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200) {
      var add_like = JSON.parse(this.responseText).like
      $("#likes"+i).html(add_like)
    }   
  }
  xhttp.open("POST", "/like-book/")
  xhttp.send(result);
}

function dislikeBook(i) {
  var result = JSON.stringify(books[i]);
  var xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200) {
      var add_like = JSON.parse(this.responseText).like
      $("#likes"+i).html(add_like)
    }   
  }
  xhttp.open("POST", "/dislike-book/")
  xhttp.send(result);
}

function topBooks() {
  var xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200) {
      var book = JSON.parse(this.responseText).book_list
      $('.modal-body').html('')
      for (var i = 0; i < 5; i++) {
        $('.modal-body').append(
          "<div id='box' class='row'><div class='col-sm-6' style='text-align:right;'>" +
          "<img src='" + book[i]['image_link'] + "' style:'width:30px; height:50px;'></img> </div>" +
          "<div class='col-sm'> <br> <p style='text-align:left;'>" + "Title: " + book[i]['title'] + "</p>" +
          "<p id='like-count' style='text-align:left;'>" + "No of likes: " + book[i]['like'] + "</p></div></div>" + "<br>"
        )
      }
    }
  }
  xhttp.open("GET", "/top-5-books/", true);
  xhttp.send();
}

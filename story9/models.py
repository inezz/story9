from django.db import models

# Create your models here.
class Books(models.Model):
    book_id = models.CharField(max_length=100, unique=True)
    image_link = models.TextField()
    title = models.CharField(max_length=128)
    authors = models.CharField(max_length=100, null=True, default="Unknown")
    publisher = models.CharField(max_length=1000,null=True, default="Unknown")
    like_count = models.IntegerField(default=0)

    def __str__(self):
        return self.title
